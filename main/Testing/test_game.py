import io
import unittest
from unittest import TestCase, mock

from main import Game
from main.Testing.UtilitiesForTest import create_dice

"""
Automated testing currently incomplete for the entire game, but the scorecard logic is sound.
"""


class TestGame(TestCase):

    def test_perform_rolls(self):
        self.fail()

    def test_perform_individual_roll(self):
        self.fail()

    def test_roll_dice(self):
        dice = create_dice([7, 7, 7, 7, 7], [True, True, False, False, True])
        Game.roll_dice(dice)
        assert dice[0].value is 7
        assert dice[1].value is 7
        assert dice[2].value in [1, 2, 3, 4, 5, 6]
        assert dice[3].value in [1, 2, 3, 4, 5, 6]
        assert dice[4].value is 7

    def test_begin_game(self):
        self.fail()

    def test_unlocked_dice_display(self):
        expected_output = ["|   | |  x| |  x| |x x| |x x| ",
                           "| x | |   | | x | |   | | x | ",
                           "|   | |x  | |x  | |x x| |x x| "]

        self.assert_stdout(create_dice([1, 2, 3, 4, 5], [False, False, False, False, False]), expected_output)

    def test_some_locked_dice_display(self):
        expected_output = ["|   | |  o| |  o| |o o| |x x| ",
                           "| x | |   | | o | |o o| | x | ",
                           "|   | |o  | |o  | |o o| |x x| "]

        dice = create_dice([1, 2, 3, 6, 5], [False, True, True, True, False])
        self.assert_stdout(dice, expected_output)

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def assert_stdout(self, test_input, expected_outputs, mock_stdout):
        Game.display_dice(test_input)
        for expected_output in expected_outputs:
            self.assertIn(expected_output, mock_stdout.getvalue())
