from unittest import TestCase

from main.Items.Die import Die


class TestDie(TestCase):
    def test_initial_values(self):
        die = Die()
        assert die.value == 1
        assert die.is_locked is False

    def test_lock(self):
        die = Die()
        die.is_locked = True
        assert die.is_locked is True
        die.unlock_die()
        assert die.is_locked is False

    def test_unlock(self):
        die = Die()
        die.is_locked = False
        assert die.is_locked is False
        die.lock_die()
        assert die.is_locked is True

    def test_roll(self):
        die = Die()
        die.value = 7
        assert die.value is 7
        die.roll()
        assert die.value in [1, 2, 3, 4, 5, 6]
