from unittest import TestCase

from main.Items.Scorecard import Scorecard
from main.Testing.UtilitiesForTest import create_dice

all_unlocked = [False, False, False, False, False]


class TestScorecard(TestCase):
    def test_play_ones(self):
        scorecard = Scorecard()
        assert scorecard.ones is None
        scorecard.play_ones(create_dice([1, 4, 3, 4, 1], all_unlocked))
        assert scorecard.ones is 2

    def test_play_twos(self):
        scorecard = Scorecard()
        assert scorecard.twos is None
        scorecard.play_twos(create_dice([2, 4, 2, 2, 1], all_unlocked))
        assert scorecard.twos is 6

    def test_play_threes(self):
        scorecard = Scorecard()
        assert scorecard.threes is None
        scorecard.play_threes(create_dice([2, 3, 2, 2, 1], all_unlocked))
        assert scorecard.threes is 3

    def test_play_fours(self):
        scorecard = Scorecard()
        assert scorecard.fours is None
        scorecard.play_fours(create_dice([4, 4, 4, 4, 1], all_unlocked))
        assert scorecard.fours is 16

    def test_play_fives(self):
        scorecard = Scorecard()
        assert scorecard.fives is None
        scorecard.play_fives(create_dice([5, 5, 2, 2, 1], all_unlocked))
        assert scorecard.fives is 10

    def test_play_sixes(self):
        scorecard = Scorecard()
        assert scorecard.sixes is None
        scorecard.play_sixes(create_dice([2, 6, 5, 2, 1], all_unlocked))
        assert scorecard.sixes is 6

    def test_play_three_of_a_kind(self):
        scorecard = Scorecard()
        assert scorecard.three_of_a_kind is None
        scorecard.play_three_of_a_kind(create_dice([2, 4, 2, 2, 1], all_unlocked))
        assert scorecard.three_of_a_kind is 11

    def test_play_three_of_a_kind_missing(self):
        scorecard = Scorecard()
        assert scorecard.three_of_a_kind is None
        scorecard.play_three_of_a_kind(create_dice([2, 4, 4, 2, 1], all_unlocked))
        assert scorecard.three_of_a_kind is 0

    def test_play_four_of_a_kind(self):
        scorecard = Scorecard()
        assert scorecard.four_of_a_kind is None
        scorecard.play_four_of_a_kind(create_dice([4, 4, 4, 4, 1], all_unlocked))
        assert scorecard.four_of_a_kind is 17

    def test_play_four_of_a_kind_missing(self):
        scorecard = Scorecard()
        assert scorecard.four_of_a_kind is None
        scorecard.play_four_of_a_kind(create_dice([4, 4, 4, 1, 1], all_unlocked))
        assert scorecard.four_of_a_kind is 0

    def test_play_full_house(self):
        scorecard = Scorecard()
        assert scorecard.full_house is None
        scorecard.play_full_house(create_dice([4, 4, 4, 1, 1], all_unlocked))
        assert scorecard.full_house is 25

    def test_play_full_house_missing(self):
        scorecard = Scorecard()
        assert scorecard.full_house is None
        scorecard.play_full_house(create_dice([2, 4, 4, 1, 1], all_unlocked))
        assert scorecard.full_house is 0

    def test_play_small_straight_front(self):
        scorecard = Scorecard()
        assert scorecard.small_straight is None
        scorecard.play_small_straight(create_dice([1, 2, 3, 4, 1], all_unlocked))
        assert scorecard.small_straight is 30

    def test_play_small_straight_middle(self):
        scorecard = Scorecard()
        assert scorecard.small_straight is None
        scorecard.play_small_straight(create_dice([2, 3, 4, 5, 2], all_unlocked))
        assert scorecard.small_straight is 30

    def test_play_small_straight_last(self):
        scorecard = Scorecard()
        assert scorecard.small_straight is None
        scorecard.play_small_straight(create_dice([3, 4, 5, 6, 3], all_unlocked))
        assert scorecard.small_straight is 30

    def test_play_small_straight_missing(self):
        scorecard = Scorecard()
        assert scorecard.small_straight is None
        scorecard.play_small_straight(create_dice([3, 3, 5, 6, 3], all_unlocked))
        assert scorecard.small_straight is 0

    def test_play_large_straight_front(self):
        scorecard = Scorecard()
        assert scorecard.large_straight is None
        scorecard.play_large_straight(create_dice([1, 2, 3, 4, 5], all_unlocked))
        assert scorecard.large_straight is 40

    def test_play_large_straight_last(self):
        scorecard = Scorecard()
        assert scorecard.large_straight is None
        scorecard.play_large_straight(create_dice([2, 3, 4, 5, 6], all_unlocked))
        assert scorecard.large_straight is 40

    def test_play_large_straight_missing(self):
        scorecard = Scorecard()
        assert scorecard.large_straight is None
        scorecard.play_large_straight(create_dice([2, 3, 3, 5, 6], all_unlocked))
        assert scorecard.large_straight is 0

    def test_play_yahtzee(self):
        scorecard = Scorecard()
        assert scorecard.yahtzee is None
        scorecard.play_yahtzee(create_dice([1, 1, 1, 1, 1], all_unlocked))
        assert scorecard.yahtzee is 50

    def test_play_yahtzee_missing(self):
        scorecard = Scorecard()
        assert scorecard.yahtzee is None
        scorecard.play_yahtzee(create_dice([2, 1, 1, 1, 1], all_unlocked))
        assert scorecard.yahtzee is 0

    def test_play_chance(self):
        scorecard = Scorecard()
        assert scorecard.chance is None
        scorecard.play_chance(create_dice([1, 4, 1, 3, 1], all_unlocked))
        assert scorecard.chance is 10

    def test_get_score(self):
        scorecard = Scorecard()
        scorecard.ones = 3
        scorecard.twos = 6
        scorecard.threes = 9
        scorecard.fours = 12
        scorecard.fives = 15
        scorecard.sixes = 18

        scorecard.three_of_a_kind = 15
        scorecard.four_of_a_kind = 20
        scorecard.full_house = 0
        scorecard.small_straight = 30
        scorecard.large_straight = 40
        scorecard.yahtzee = 0
        scorecard.chance = 10
        assert scorecard.get_score() is 213

    def test_get_score_no_bonus(self):
        scorecard = Scorecard()
        scorecard.ones = 2
        scorecard.twos = 6
        scorecard.threes = 9
        scorecard.fours = 12
        scorecard.fives = 15
        scorecard.sixes = 18

        scorecard.three_of_a_kind = 15
        scorecard.four_of_a_kind = 20
        scorecard.full_house = 0
        scorecard.small_straight = 30
        scorecard.large_straight = 40
        scorecard.yahtzee = 0
        scorecard.chance = 10
        assert scorecard.get_score() is 177

    def test_game_over_false(self):
        assert Scorecard().game_over() is False

    def test_game_over_true(self):
        scorecard = Scorecard()
        scorecard.ones = 2
        scorecard.twos = 6
        scorecard.threes = 9
        scorecard.fours = 12
        scorecard.fives = 15
        scorecard.sixes = 18

        scorecard.three_of_a_kind = 15
        scorecard.four_of_a_kind = 20
        scorecard.full_house = 0
        scorecard.small_straight = 30
        scorecard.large_straight = 40
        scorecard.yahtzee = 0
        scorecard.chance = 10
        assert scorecard.game_over() is True

    # def test_play_roll(self):
    #     self.fail()

    def test_count_dice(self):
        counter = Scorecard().count_dice(create_dice([1, 3, 3, 3, 5], all_unlocked))
        assert counter.get(1) is 1
        assert counter.get(2) is 0
        assert counter.get(3) is 3
        assert counter.get(4) is 0
        assert counter.get(5) is 1
        assert counter.get(6) is 0

    def test_total_dice(self):
        assert Scorecard().total_dice(create_dice([3, 4, 2, 2, 4], all_unlocked)) is 15
