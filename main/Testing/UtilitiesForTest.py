from main.Items.Die import Die


def create_dice(numbers, locks):
    dice = [Die(), Die(), Die(), Die(), Die()]
    dice[0].value = numbers[0]
    dice[1].value = numbers[1]
    dice[2].value = numbers[2]
    dice[3].value = numbers[3]
    dice[4].value = numbers[4]

    dice[0].lock_die() if locks[0] else dice[0].unlock_die()
    dice[1].lock_die() if locks[1] else dice[1].unlock_die()
    dice[2].lock_die() if locks[2] else dice[2].unlock_die()
    dice[3].lock_die() if locks[3] else dice[3].unlock_die()
    dice[4].lock_die() if locks[4] else dice[4].unlock_die()
    return dice
