import sys

import boto3

from main.Items.Die import Die
from main.Items.Scorecard import Scorecard

empty = "|   | "
right_open = "|  x| "
sides_open = "|x x| "
middle_open = "| x | "
left_open = "|x  | "
right_locked = "|  o| "
sides_locked = "|o o| "
middle_locked = "| o | "
left_locked = "|o  | "

first_row_open = [empty, right_open, right_open, sides_open, sides_open, sides_open]
second_row_open = [middle_open, empty, middle_open, empty, middle_open, sides_open]
third_row_open = [empty, left_open, left_open, sides_open, sides_open, sides_open]
all_rows_open = [first_row_open, second_row_open, third_row_open]

first_row_locked = [empty, right_locked, right_locked, sides_locked, sides_locked, sides_locked]
second_row_locked = [middle_locked, empty, middle_locked, empty, middle_locked, sides_locked]
third_row_locked = [empty, left_locked, left_locked, sides_locked, sides_locked, sides_locked]
all_rows_locked = [first_row_locked, second_row_locked, third_row_locked]

msg_mid_game_options = "enter 0 to display your score\n" + \
                       "enter 1-5  to lock or unlock the dice in your hand\n" + \
                       "enter 6 to roll the dice\n" + \
                       "enter 7 to score your dice\n" + \
                       "enter 8 to save your game\n> "
msg_invalid_input = "You have entered invalid input. Please try again"
msg_scoring = "enter the number that corresponds with where you want to score your dice\n> "
msg_begin = "enter 1 to start the game \nenter 2 to load a previous game\nenter 3 to exit\n> "
msg_welcome = "Welcome to Yahtzee!"
msg_total = "your total was {}"
msg_game_name_insert = "enter a super secret name so that you can use it to load your game later\n> "
msg_game_name_load = "enter the super secret name so that you can retrieve your game\nenter 1 to exit\n> "
msg_game_not_found = "couldn't find a gem with that name\nplease try again"


def unlock_all_dice(dice):
    for die in dice:
        die.unlock_die()


def perform_scoring(dice, scorecard):
    done_scoring = False
    while not done_scoring:
        try:
            user_input = int(input(msg_scoring))
        except ValueError:
            user_input = 14
        if user_input in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]:
            done_scoring = scorecard.play_roll(user_input, dice)


def roll_dice(dice):
    for die in dice:
        if die.is_locked is not True:
            die.roll()


def begin_game(dice):
    user_input = None
    while user_input not in [1, 2, 3]:
        print(msg_begin)
        if user_input is not None:
            print(msg_invalid_input)
        try:
            user_input = int(input())
        except ValueError:
            user_input = -1
    if user_input is 1:
        return Scorecard()
    if user_input is 2:
        return load_game(dice)
    return None


def display_dice(dice):
    for y in range(3):
        for die in dice:
            if die.is_locked:
                sys.stdout.write(all_rows_locked[y][die.value - 1])
            else:
                sys.stdout.write(all_rows_open[y][die.value - 1])
        print()


def perform_individual_roll(dice, roll, scorecard):
    while True:
        try:
            user_input = int(input(msg_mid_game_options)) - 1
        except ValueError:
            user_input = 1000
        if user_input is -1:
            scorecard.display()
            display_dice(dice)
        elif user_input in [0, 1, 2, 3, 4]:
            if dice[user_input].is_locked:
                dice[user_input].unlock_die()
            else:
                dice[user_input].lock_die()
            display_dice(dice)
        elif user_input is 5:
            roll_dice(dice)
            return roll
        elif user_input is 6:
            return 3
        elif user_input is 7:
            save_game(scorecard, dice, get_scorecard_table())
        else:
            print(msg_invalid_input)


def get_scorecard_table():
    dynamodb = boto3.resource('dynamodb')
    try:
        return database_initialize(dynamodb)
    except Exception:
        print("table exists")
        return dynamodb.Table('Scorecards')


def perform_rolls(dice, scorecard):
    roll_dice(dice)
    roll = 0
    while roll < 2:
        display_dice(dice)
        roll = perform_individual_roll(dice, roll, scorecard)
        roll += 1
    unlock_all_dice(dice)


def take_turn(scorecard, dice):
    perform_rolls(dice, scorecard)
    scorecard.display()
    display_dice(dice)
    perform_scoring(dice, scorecard)


def save_game(scorecard, dice, table):
    table.put_item(
        Item={
            'GameName': get_game_name_from_user_for_insert(table),
            'ones': str(scorecard.ones),
            'twos': str(scorecard.twos),
            'threes': str(scorecard.threes),
            'fours': str(scorecard.fours),
            'fives': str(scorecard.fives),
            'sixes': str(scorecard.sixes),
            'threeofakind': str(scorecard.three_of_a_kind),
            'fourofakind': str(scorecard.four_of_a_kind),
            'fullhouse': str(scorecard.full_house),
            'smallstraight': str(scorecard.small_straight),
            'largestraight': str(scorecard.large_straight),
            'yahtzee': str(scorecard.yahtzee),
            'chance': str(scorecard.chance),
            'dice1value': str(dice[0].value),
            'dice1locked': str(dice[0].is_locked),
            'dice2value': str(dice[1].value),
            'dice2locked': str(dice[1].is_locked),
            'dice3value': str(dice[2].value),
            'dice3locked': str(dice[2].is_locked),
            'dice4value': str(dice[3].value),
            'dice4locked': str(dice[3].is_locked),
            'dice5value': str(dice[4].value),
            'dice5locked': str(dice[4].is_locked)
        }
    )

    sys.exit()


def get_game_name_from_user_for_insert(table):
    while True:
        user_input = input(msg_game_name_insert)
        try:
            table.get_item(
                Key={
                    'GameName': user_input
                }
            )["Item"]
        except KeyError:
            return user_input

        print(msg_invalid_input)


def get_item_for_load(table):
    while True:
        user_input = input(msg_game_name_load)
        if user_input is "1":
            return None
        try:
            item = table.get_item(
                Key={
                    'GameName': user_input
                }
            )["Item"]
        except KeyError:
            print(msg_game_not_found)
            continue

        if item is not None:
            return item
        else:
            print(msg_invalid_input)


def load_game(dice):
    item = get_item_for_load(get_scorecard_table())
    if item is None:
        return Scorecard()
    else:
        return populate_scorecard(item, dice)


def populate_scorecard(item, dice):
    scorecard = Scorecard()
    scorecard.ones = int_or_none(item.get('ones'))
    scorecard.twos = int_or_none(item.get('twos'))
    scorecard.threes = int_or_none(item.get('threes'))
    scorecard.fours = int_or_none(item.get('fours'))
    scorecard.fives = int_or_none(item.get('fives'))
    scorecard.sixes = int_or_none(item.get('sixes'))
    scorecard.three_of_a_kind = int_or_none(item.get('threeofakind'))
    scorecard.four_of_a_kind = int_or_none(item.get('fourofakind'))
    scorecard.full_house = int_or_none(item.get('fullhouse'))
    scorecard.small_straight = int_or_none(item.get('smallstraight'))
    scorecard.large_straight = int_or_none(item.get('largestraight'))
    scorecard.yahtzee = int_or_none(item.get('yahtzee'))
    scorecard.chance = int_or_none(item.get('chance'))
    dice[0].value = int_or_none(item.get('dice1value'))
    dice[0].is_locked = determine_boolean(item.get('dice1locked'))
    dice[1].value = int_or_none(item.get('dice2value'))
    dice[1].is_locked = determine_boolean(item.get('dice2locked'))
    dice[2].value = int_or_none(item.get('dice3value'))
    dice[2].is_locked = determine_boolean(item.get('dice3locked'))
    dice[3].value = int_or_none(item.get('dice4value'))
    dice[3].is_locked = determine_boolean(item.get('dice4locked'))
    dice[4].value = int_or_none(item.get('dice5value'))
    dice[4].is_locked = determine_boolean(item.get('dice5locked'))
    return scorecard


def int_or_none(item_input):
    if item_input == 'None':
        return None
    else:
        return int(str(item_input))


def determine_boolean(item_input):
    return item_input == "True"


def database_initialize(dynamodb):
    table = dynamodb.create_table(
        TableName='Scorecards',
        KeySchema=[
            {
                'AttributeName': 'GameName',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'GameName',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    table.meta.client.get_waiter('table_exists').wait(TableName='Scorecards')
    print("Database successfully initialized")
    return table


def play_game():
    print(msg_welcome)
    dice = [Die(), Die(), Die(), Die(), Die()]
    scorecard = begin_game(dice)

    if scorecard is not None:
        while scorecard.game_over() is not True:
            take_turn(scorecard, dice)
        scorecard.display()
        print(msg_total.format(scorecard.get_score()))
