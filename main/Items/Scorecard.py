class Scorecard:
    ones = None
    twos = None
    threes = None
    fours = None
    fives = None
    sixes = None

    three_of_a_kind = None
    four_of_a_kind = None
    full_house = None
    small_straight = None
    large_straight = None
    yahtzee = None
    chance = None

    def play_ones(self, dice):
        if self.ones is not None:
            return False

        self.ones = 0
        for die in dice:
            if die.value == 1:
                self.ones += 1
        return True

    def play_twos(self, dice):
        if self.twos is not None:
            return False

        self.twos = 0
        for die in dice:
            if die.value == 2:
                self.twos += 2
        return True

    def play_threes(self, dice):
        if self.threes is not None:
            return False

        self.threes = 0
        for die in dice:
            if die.value == 3:
                self.threes += 3
        return True

    def play_fours(self, dice):
        if self.fours is not None:
            return False

        self.fours = 0
        for die in dice:
            if die.value == 4:
                self.fours += 4
        return True

    def play_fives(self, dice):
        if self.fives is not None:
            return False

        self.fives = 0
        for die in dice:
            if die.value == 5:
                self.fives += 5
        return True

    def play_sixes(self, dice):
        if self.sixes is not None:
            return False

        self.sixes = 0
        for die in dice:
            if die.value == 6:
                self.sixes += 6
        return True

    def play_three_of_a_kind(self, dice):
        if self.three_of_a_kind is not None:
            return False

        self.three_of_a_kind = 0

        for count in self.count_dice(dice).values():
            if count >= 3:
                self.three_of_a_kind = self.total_dice(dice)
                break
        return True

    def play_four_of_a_kind(self, dice):
        if self.four_of_a_kind is not None:
            return False

        self.four_of_a_kind = 0

        for count in self.count_dice(dice).values():
            if count >= 4:
                self.four_of_a_kind = self.total_dice(dice)
                break
        return True

    def play_full_house(self, dice):
        if self.full_house is not None:
            return False

        self.full_house = 0

        two_count = False
        three_count = False
        for count in self.count_dice(dice).values():
            if count == 2:
                two_count = True
                continue
            if count == 3:
                three_count = True
                continue
        if two_count and three_count:
            self.full_house = 25
        return True

    def play_small_straight(self, dice):
        if self.small_straight is not None:
            return False

        self.small_straight = 0

        counters = self.count_dice(dice)
        if counters[3] > 0 and counters[4] > 0:
            if (counters[2] > 0 and counters[5] > 0) \
                    or (counters[1] > 0 and counters[2] > 0) \
                    or (counters[5] > 0 and counters[6] > 0):
                self.small_straight = 30
        return True

    def play_large_straight(self, dice):
        if self.large_straight is not None:
            return False

        self.large_straight = 0

        counters = self.count_dice(dice)
        if counters[2] > 0 and counters[3] > 0 and counters[4] > 0 and counters[5] > 0:
            if counters[1] > 0 or counters[6] > 0:
                self.large_straight = 40
        return True

    def play_yahtzee(self, dice):
        if self.yahtzee is not None:
            return False

        value = dice[0].value
        fail = False
        for die in dice:
            if die.value != value:
                fail = True

        if not fail:
            self.yahtzee = 50
        else:
            self.yahtzee = 0
        return True

    def play_chance(self, dice):
        if self.chance is not None:
            return False

        self.chance = self.total_dice(dice)
        return True

    def get_score(self):
        top_score = self.ones + self.twos + self.threes + self.fours + self.fives + self.sixes

        if top_score >= 63:
            top_score += 35

        bottom_score = self.three_of_a_kind \
                       + self.four_of_a_kind \
                       + self.full_house \
                       + self.small_straight \
                       + self.large_straight \
                       + self.yahtzee \
                       + self.chance

        return top_score + bottom_score

    def game_over(self):
        return self.ones is not None and \
               self.twos is not None and \
               self.threes is not None and \
               self.fours is not None and \
               self.fives is not None and \
               self.sixes is not None and \
               self.three_of_a_kind is not None and \
               self.four_of_a_kind is not None and \
               self.full_house is not None and \
               self.small_straight is not None and \
               self.large_straight is not None and \
               self.yahtzee is not None and \
               self.chance is not None

    def display(self):
        score = "1: ones - {}\n" \
                "2: twos - {}\n" \
                "3: threes - {}\n" \
                "4: fours - {}\n" \
                "5: fives - {}\n" \
                "6: sixes - {}\n" \
                "7: three of a kind - {}\n" \
                "8: four of a kind - {}\n" \
                "9: full house - {}\n" \
                "10: small straight - {}\n" \
                "11: large straight - {}\n" \
                "12: yahtzee - {}\n" \
                "13: chance - {}\n"
        print(score.format(self.ones,
                           self.twos,
                           self.threes,
                           self.fours,
                           self.fives,
                           self.sixes,
                           self.three_of_a_kind,
                           self.four_of_a_kind,
                           self.full_house,
                           self.small_straight,
                           self.large_straight,
                           self.yahtzee,
                           self.chance))

    def play_roll(self, selection, dice):
        if selection is 1:
            return self.play_ones(dice)
        elif selection is 2:
            return self.play_twos(dice)
        elif selection is 3:
            return self.play_threes(dice)
        elif selection is 4:
            return self.play_fours(dice)
        elif selection is 5:
            return self.play_fives(dice)
        elif selection is 6:
            return self.play_sixes(dice)
        elif selection is 7:
            return self.play_three_of_a_kind(dice)
        elif selection is 8:
            return self.play_four_of_a_kind(dice)
        elif selection is 9:
            return self.play_full_house(dice)
        elif selection is 10:
            return self.play_small_straight(dice)
        elif selection is 11:
            return self.play_large_straight(dice)
        elif selection is 12:
            return self.play_yahtzee(dice)
        elif selection is 13:
            return self.play_chance(dice)
        else:
            return False

    @staticmethod
    def count_dice(dice):
        counters = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}

        for die in dice:
            if 0 < die.value < 7:
                counters[die.value] += 1
        return counters

    @staticmethod
    def total_dice(dice):
        total = 0
        for die in dice:
            total += die.value
        return total
