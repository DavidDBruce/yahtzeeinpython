import random


class Die:
    value = 1
    is_locked = False

    def lock_die(self):
        self.is_locked = True

    def unlock_die(self):
        self.is_locked = False

    def roll(self):
        self.value = random.randrange(1, 7)
        return self.value
